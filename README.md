# Introduction

Conf file for a great zsh install based on oh-my-zsh
And a vim env for python dev

# Installation

* Install the repo
git clone git@bitbucket.org:aheimburger/dotfiles.git

* Install the sub module for oh-my-zsh
git submodule update --init

* Create sym links
ln -s dotfiles/zsh/zshrc ~/.zshrc
ln -s dotfiles/zsh/oh-my-zsh ~/.oh-my-zsh
ln -s dotfiles/vim/vimrc ~/.vimrc
ln -s dotfiles/vim/vim ~/.vim
